import Http from '../../App/Http'

enum Urls {
  Root = '/donation',
}

class DonationService {
  public static findAll() {
    return Http.get<Donation[]>(Urls.Root)
  }

  public static findById(id: number) {
    return Http.get<Donation>(`${Urls.Root}/${id}`)
  }

  public static create(donation: DonationCreateDto) {
    const formData = new FormData()

    Object.entries(donation).forEach(([key, value]) => {
      if (value) {
        formData.append(key, value instanceof File ? value : value.toString())
      }
    })

    return Http.post<{ id: number }>(Urls.Root, formData)
  }
}

export interface Donation {
  id: number
  imageName: string
  image: File | undefined
  name: string
  amount: number | undefined
  target: string
  description: string
  accountId: string
  authorId: string
  endDate?: string
  progress: number
}

export type DonationCreateDto = Omit<Donation, 'id' | 'imageName' | 'progress'>

export default DonationService
