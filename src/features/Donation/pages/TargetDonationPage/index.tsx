import React, { useContext } from 'react'
import Header from '../../../../components/Header'
import { Formik } from 'formik'
import * as yup from 'yup'
import useRouter from '../../../../hooks/useRouter'
import { initialTargetDonationFormValue, PanelId } from '../../../../App'
import { DonationCreateDto } from '../../DonationService'
import DonationForm from '../../components/DonationForm'

export const TargetPageContext = React.createContext<TargetPageContextValue | undefined>(undefined)

interface TargetPageContextValue {
  targetDonationFormValue: DonationCreateDto | undefined
  setTargetDonationFormValue: (value: DonationCreateDto) => void
}

const TargetDonationPage = () => {
  const router = useRouter()
  const { targetDonationFormValue, setTargetDonationFormValue } = useContext(TargetPageContext) || {}

  return (
    <>
      <Header
        onBackButtonClick={() => {
          if (setTargetDonationFormValue) {
            setTargetDonationFormValue(initialTargetDonationFormValue)
          }
        }}
      >
        Целевой сбор
      </Header>
      <Formik
        initialValues={targetDonationFormValue || {}}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          if (targetDonationFormValue && setTargetDonationFormValue) {
            setTargetDonationFormValue({
              ...targetDonationFormValue,
              ...values,
            })
          }

          router.goForward(PanelId.AdditionalSettings)
        }}
      >
        {() => (
          <DonationForm targetPlaceholder="Например, лечение человека" submitButtonText="Далее" />
        )}
      </Formik>
    </>
  )
}

const validationSchema = yup.object().shape({
  name: yup.string().required('Пожалуйста, введите название сбора'),
  amount: yup
    .number()
    .required('Пожалуйста, введите сумму сбора')
    .typeError('Пожалуйста, введите число'),
  target: yup.string().required('Пожалуйста, введите цель сбора'),
  description: yup.string().required('Пожалуйста, введите описание сбора'),
  accountId: yup.string().required('Пожалуйста, выберите счёт сбора'),
})

export default TargetDonationPage
