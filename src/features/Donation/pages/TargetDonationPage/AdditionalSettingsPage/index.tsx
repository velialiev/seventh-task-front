import React, { FormEvent, useContext, useState } from 'react'
import Header from '../../../../../components/Header'
import {
  Button, FormLayout, FormLayoutGroup, Radio, 
} from '@vkontakte/vkui'
import FormInput from '../../../../../components/FormInput'
import generateValidationProps from '../../../../../utils/generateValidationProps'
import FormSelect from '../../../../../components/FormSelect'
import { Formik } from 'formik'
import * as yup from 'yup'
import useRouter from '../../../../../hooks/useRouter'
import { TargetPageContext } from '../index'
import DonationService, { DonationCreateDto } from '../../../DonationService'
import { initialTargetDonationFormValue, PanelId } from '../../../../../App'
import mockAuthors from '../../../../../mocks/mockAuthors'

const AdditionalSettingsPage = () => {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)

  const { targetDonationFormValue, setTargetDonationFormValue } = useContext(TargetPageContext) || {}

  const [endType, setEndType] = useState<EndType>(
    targetDonationFormValue?.endDate ? EndType.UntilDate : EndType.UntilCollectAmount,
  )

  return (
    <>
      <Formik
        initialValues={targetDonationFormValue || {}}
        validationSchema={
          endType === EndType.UntilDate
            ? untilDateValidationSchema
            : untilCollectAmountValidationSchema
        }
        onSubmit={async (values) => {
          setIsLoading(true)
          await DonationService.create(values as DonationCreateDto)
          setIsLoading(false)

          if (setTargetDonationFormValue) {
            setTargetDonationFormValue(initialTargetDonationFormValue)
          }

          router.goForward(PanelId.Donations)
        }}
      >
        {({
          errors, touched, handleSubmit, values, setValues, 
        }) => (
          <>
            <Header
              onBackButtonClick={() => {
                if (targetDonationFormValue && setTargetDonationFormValue) {
                  setTargetDonationFormValue({
                    ...targetDonationFormValue,
                    ...values,
                  })
                }
              }}
            >
              Дополнительно
            </Header>
            <FormLayout onSubmit={(e) => handleSubmit(e as FormEvent<HTMLFormElement>)}>
              <FormSelect
                name="authorId"
                options={mockAuthors}
                resolveId={(item) => item.id}
                resolveContent={(item) => item.name}
                top="Автор"
                placeholder="Кто создатель сбора?"
                {...generateValidationProps('authorId', errors, touched)}
              />

              <FormLayoutGroup top="Сбор завершится">
                <Radio
                  name="endType"
                  value={EndType.UntilCollectAmount}
                  checked={endType === EndType.UntilCollectAmount}
                  onChange={(e) => {
                    setEndType(e.target.value as EndType)
                    setValues({
                      ...values,
                      endDate: '',
                    })
                  }}
                >
                  Когда соберём сумму
                </Radio>
                <Radio
                  name="endType"
                  value={EndType.UntilDate}
                  checked={endType === EndType.UntilDate}
                  onChange={(e) => setEndType(e.target.value as EndType)}
                >
                  В определённую дату
                </Radio>
              </FormLayoutGroup>

              {endType === EndType.UntilDate && (
              <FormInput
                name="endDate"
                type="date"
                {...generateValidationProps('endDate', errors, touched)}
              />
              )}

              <Button size="xl" type="submit">
                Создать сбор
              </Button>
            </FormLayout>
          </>
        )}
      </Formik>
    </>
  )
}

const untilCollectAmountValidationSchema = yup.object().shape({
  authorId: yup.string().required('Пожалуйста, выберите автора'),
})

const untilDateValidationSchema = yup.object().shape({
  authorId: yup.string().required('Пожалуйста, выберите автора'),
  endDate: yup.string().required('Пожалуйста, введите дату окончания сбора'),
})

enum EndType {
  UntilCollectAmount = 'UntilCollectAmount',
  UntilDate = 'UntilDate',
}

export default AdditionalSettingsPage
