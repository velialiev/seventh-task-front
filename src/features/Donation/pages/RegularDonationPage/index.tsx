import React, { useState } from 'react'
import useRouter from '../../../../hooks/useRouter'
import Header from '../../../../components/Header'
import { PanelId } from '../../../../App'
import DonationForm from '../../components/DonationForm'
import { Formik } from 'formik'
import * as yup from 'yup'
import DonationService, { DonationCreateDto } from '../../DonationService'

const RegularDonationPage = () => {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)

  return (
    <>
      <Header>Регулярный сбор</Header>
      <Formik
        initialValues={{
          name: '',
          amount: undefined,
          target: '',
          description: '',
          accountId: '',
          authorId: '',
        }}
        validationSchema={validationSchema}
        onSubmit={async (values) => {
          setIsLoading(true)
          await DonationService.create(values as DonationCreateDto)
          setIsLoading(false)
          router.goForward(PanelId.Donations)
        }}
      >
        {() => (
          <DonationForm
            targetPlaceholder="Например, поддержка приюта"
            submitButtonText="Создать сбор"
            showAuthorField
          />
        )}
      </Formik>
    </>
  )
}

const validationSchema = yup.object().shape({
  name: yup.string().required('Пожалуйста, введите название сбора'),
  amount: yup
    .number()
    .required('Пожалуйста, введите сумму сбора')
    .typeError('Пожалуйста, введите число'),
  target: yup.string().required('Пожалуйста, введите цель сбора'),
  description: yup.string().required('Пожалуйста, введите описание сбора'),
  accountId: yup.string().required('Пожалуйста, выберите счёт сбора'),
  authorId: yup.string().required('Пожалуйста, выберите автора'),
})

export default RegularDonationPage
