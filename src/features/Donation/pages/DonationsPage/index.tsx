import React, { useEffect, useState } from 'react'
import {
  Button,
  Card,
  Div,
  FixedLayout,
  Group,
  Placeholder,
  Progress,
  Separator,
  Spinner,
  Text,
  Title,
} from '@vkontakte/vkui'
import './index.scss'
import useRouter from '../../../../hooks/useRouter'
import { PanelId } from '../../../../App'
import Header from '../../../../components/Header'
import DonationService, { Donation } from '../../DonationService'
import mockAuthors from '../../../../mocks/mockAuthors'
import nophoto from '../../../../assets/images/nophoto.jpg'
import cn from 'classnames'
import getRandomIntInclusive from '../../../../utils/getRandomIntInclusive'

export const getAuthorById = (id: string) => {
  return mockAuthors.find((author) => author.id === id)
}

export const formatDate = (date: string | undefined) => {
  if (!date) {
    return 'Дата не указана'
  }

  const dateObj = new Date(date)

  if (isNaN(+dateObj)) {
    return 'Дата не указана'
  }

  return `До ${dateObj.toLocaleDateString()}`
}

const DonationsPage = () => {
  const router = useRouter()
  const [donations, setDonations] = useState<Donation[]>([])
  const [loading, setLoading] = useState(false)

  const help = (donationId: number) => {
    setDonations(
      donations.map((donation) => {
        if (donation.id === donationId) {
          return {
            ...donation,
            progress: Math.min((donation.progress || 0) + getRandomIntInclusive(3, 20), 100),
          }
        }

        return donation
      }),
    )
  }

  const renderCurrentProgress = (progress: number, amount: number | undefined) => {
    if (!progress) {
      return 'Помогите первым'
    }

    if (!amount) {
      return 'Сумма сбора не указана'
    }

    if (progress !== 100) {
      return `Собрано ${Math.round(amount * (progress / 100))} ₽ из ${amount} ₽`
    }

    return `${amount} ₽ собраны!`
  }

  const resolveProgressBarClassName = (progress: number) => {
    return cn({
      'Snippet__progress-bar--full': progress === 100,
    })
  }

  const renderDonations = () => {
    if (loading || !donations?.length) {
      return (
        <Placeholder
          stretched
          action={(
            <Button onClick={() => router.goForward(PanelId.DonationTypes)} size="l">
              Создать сбор
            </Button>
          )}
        >
          {!loading ? (
            <>
              <Text className="DonationsPage__text" weight="regular">
                У Вас пока нет сборов.
              </Text>
              <Text className="DonationsPage__text" weight="regular">
                Начните доброе дело.
              </Text>
            </>
          ) : (
            <Spinner />
          )}
        </Placeholder>
      )
    }

    return (
      <Div className="Snippets">
        {donations.map((donation) => (
          <Card
            onClick={() => router.goForward(PanelId.Donation, { id: donation.id })}
            key={donation.id}
            className="Snippets__snippet Snippet"
            size="l"
            mode="outline"
          >
            <img
              src={
                donation.imageName
                  ? `${process.env.REACT_APP_BACKEND_HOST}/donation/image/${donation.imageName}`
                  : nophoto
              }
              alt=""
              className="Snippet__image"
            />
            <div className="Snippet__content">
              <Title level="3" weight="semibold">
                {donation.name}
              </Title>
              <Text className="DonationsPage__text" weight="regular">
                {getAuthorById(donation.authorId)?.name} ·{' '}
                {donation.endDate ? formatDate(donation.endDate) : 'Помощь нужна каждый месяц'}
              </Text>

              <Separator className="Snippet__separator" />

              <div className="Footer">
                <div className="Footer__progress">
                  <Text weight="regular" className="Snippet__progress-description">
                    {renderCurrentProgress(donation.progress, donation.amount)}
                  </Text>

                  <Progress
                    className={resolveProgressBarClassName(donation.progress)}
                    value={donation.progress}
                  />
                </div>

                <Button
                  disabled={donation.progress === 100}
                  mode="outline"
                  className="Footer__help-button"
                  onClick={(e) => {
                    e.stopPropagation()
                    help(donation.id)
                  }}
                >
                  Помочь
                </Button>
              </div>
            </div>
          </Card>
        ))}
        <FixedLayout className="Snippets__fixed-footer" vertical="bottom" filled>
          <Button
            className="Snippets__create-button"
            onClick={() => {
              router.goForward(PanelId.DonationTypes)

              // todo: единственный найденный workaround для https://github.com/VKCOM/VKUI/issues/1013
              window.scroll(0, 0)
            }}
            size="l"
          >
            Создать сбор
          </Button>
        </FixedLayout>
      </Div>
    )
  }

  useEffect(() => {
    (async () => {
      setLoading(true)

      try {
        const {
          data: { data },
        } = await DonationService.findAll()
        setDonations(data)
      } catch (e) {
        console.log(e)
      }

      setLoading(false)
    })()
  }, [])

  return (
    <div className="DonationsPage">
      <Header hideBackButton>Пожертвования</Header>
      <Group>{renderDonations()}</Group>
    </div>
  )
}

export default DonationsPage
