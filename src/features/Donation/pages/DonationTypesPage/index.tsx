import React from 'react'
import { Banner, Div, Group } from '@vkontakte/vkui'
import { Icon28CalendarOutline, Icon28TargetOutline } from '@vkontakte/icons'
import './index.scss'
import useRouter from '../../../../hooks/useRouter'
import { PanelId } from '../../../../App'
import Header from '../../../../components/Header'

const DonationTypesPage = () => {
  const router = useRouter()

  return (
    <div className="DonationTypesPage">
      <Header>Тип сбора</Header>
      <Group>
        <Div className="DonationTypesPage__centered">
          <Banner
            before={<Icon28TargetOutline className="DonationTypesPage__icon" />}
            header="Целевой сбор"
            subheader="Когда есть определённая цель"
            asideMode="expand"
            onClick={() => router.goForward(PanelId.TargetDonation)}
          />

          <Banner
            before={<Icon28CalendarOutline className="DonationTypesPage__icon" />}
            header="Регулярный сбор"
            subheader="Если помощь нужна ежемесячно"
            asideMode="expand"
            onClick={() => router.goForward(PanelId.RegularDonation)}
          />
        </Div>
      </Group>
    </div>
  )
}

export default DonationTypesPage
