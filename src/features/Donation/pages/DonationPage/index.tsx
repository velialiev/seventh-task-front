import React, { useEffect, useState } from 'react'
import useRouter from '../../../../hooks/useRouter'
import Header from 'src/components/Header'
import {
  Group,
  Title,
  Text,
  Separator,
  Spinner,
  Progress,
  Button,
  FixedLayout,
  Div,
} from '@vkontakte/vkui'
import DonationService, { Donation } from '../../DonationService'
import './index.scss'
import nophoto from '../../../../assets/images/nophoto.jpg'
import { formatDate, getAuthorById } from '../DonationsPage'

const DonationPage = () => {
  const router = useRouter()
  const routerData = router.data as RouterData
  const [donation, setDonation] = useState<Donation>()
  const [progress, setProgress] = useState<number>(0)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    (async () => {
      setLoading(true)

      try {
        const {
          data: { data },
        } = await DonationService.findById(routerData.id)
        setDonation(data)
      } catch (e) {
        console.log(e)
      }

      setLoading(false)
    })()
  }, [])

  useEffect(() => {
    const interval = setInterval(() => {
      setProgress((prevProgress) => {
        if (prevProgress >= 100) {
          clearInterval(interval)
        }

        return Math.min(prevProgress + 1, 100)
      })
    }, 35)
  }, [])

  const calculateCurrentAmount = () => {
    if (!donation || !donation.amount) {
      return 0
    }

    return Math.round(donation.amount * (progress / 100))
  }

  const formattedDate = donation?.endDate
    ? formatDate(donation.endDate)
    : 'Помощь нужна каждый месяц'

  const isCollected = progress === 100

  const currentAmount = calculateCurrentAmount()

  return (
    <>
      <Header>Пожертвование</Header>
      {!loading && donation ? (
        <Group className="DonationCard">
          <img
            className="DonationCard__image"
            src={
              donation.imageName
                ? `${process.env.REACT_APP_BACKEND_HOST}/donation/image/${donation.imageName}`
                : nophoto
            }
            alt=""
          />
          <div className="DonationCard__content">
            <Title className="DonationCard__name" level="1" weight="bold">
              {donation.name}
            </Title>
            <Title className="DonationCard__author" level="3" weight="medium">
              Автор {getAuthorById(donation.authorId)?.name}
            </Title>
            <Title
              className="DonationCard__date"
              level="3"
              weight="regular"
              style={{ marginBottom: 16 }}
            >
              {formattedDate}
            </Title>

            <Separator className="DonationCard__separator" />

            <Text weight="regular" className="DonationCard__target-title">
              Нужно собрать {donation.endDate ? formattedDate.toLowerCase() : 'в этом месяце'}
            </Text>

            <div className="DonationProgress">
              <div
                style={{
                  width: `${progress}%`,
                  display: progress ? 'block' : 'none',
                }}
                className="DonationProgress__current"
              >
                <span
                  style={{
                    position: 'absolute',
                    right: isCollected ? '50%' : '8px',
                    transform: isCollected ? 'translate(50%)' : 'none',
                    transition: '0.5s',
                    display: 'block',
                  }}
                >
                  {currentAmount} ₽ {isCollected ? 'собраны!' : ''}
                </span>
              </div>
              <span className="DonationProgress__target">{donation.amount} ₽</span>
            </div>

            <Separator className="DonationCard__separator" />

            <Text style={{ wordBreak: 'break-all' }} weight="regular">
              {donation.description}
            </Text>

            <Separator style={{ marginBottom: '50px' }} className="DonationCard__separator" />

            <FixedLayout vertical="bottom">
              <Div className="FixedFooter">
                <div className="DonationProgressFooter__progress">
                  <Text weight="regular" className="Snippet__progress-description">
                    {!isCollected
                      ? `Собрано ${currentAmount} ₽ из ${donation.amount} ₽`
                      : `${currentAmount} ₽ собраны!`}
                  </Text>

                  <Progress value={progress} />
                </div>

                <Button
                  disabled={isCollected}
                  size="l"
                  className="DonationProgressFooter__help-button"
                >
                  Помочь
                </Button>
              </Div>
            </FixedLayout>
          </div>
        </Group>
      ) : (
        <Spinner className="DonationSpinner" />
      )}
    </>
  )
}

interface RouterData {
  id: number
}

export default DonationPage
