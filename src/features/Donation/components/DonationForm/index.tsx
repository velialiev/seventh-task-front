import React, { FC, FormEvent } from 'react'
import FileUploader from '../../../../components/FileUploader'
import FormInput from '../../../../components/FormInput'
import generateValidationProps from '../../../../utils/generateValidationProps'
import FormSelect from '../../../../components/FormSelect'
import { Button, FormLayout, Spinner } from '@vkontakte/vkui'
import { useFormikContext } from 'formik'
import mockCards from '../../../../mocks/mockCards'
import mockAuthors from '../../../../mocks/mockAuthors'
import FormTextarea from '../../../../components/FormTextarea'

const DonationForm: FC<Props> = ({ submitButtonText, showAuthorField, targetPlaceholder }) => {
  const { errors, touched, handleSubmit } = useFormikContext()

  return (
    <FormLayout onSubmit={(e) => handleSubmit(e as FormEvent<HTMLFormElement>)}>
      <FileUploader name="image" />
      <FormInput
        name="name"
        top="Название сбора"
        placeholder="Название сбора"
        {...generateValidationProps('name', errors, touched)}
      />
      <FormInput
        name="amount"
        top="Сумма, ₽"
        placeholder="Сколько нужно собрать?"
        {...generateValidationProps('amount', errors, touched)}
      />
      <FormInput
        name="target"
        top="Цель"
        placeholder={targetPlaceholder}
        {...generateValidationProps('target', errors, touched)}
      />
      <FormTextarea
        name="description"
        top="Описание"
        placeholder="На что пойдут деньги и как они кому-то помогут?"
        {...generateValidationProps('description', errors, touched)}
      />
      <FormSelect
        name="accountId"
        options={mockCards}
        resolveId={(item) => item.id}
        resolveContent={(item) => item.name}
        top="Счёт"
        placeholder="Куда получать деньги?"
        {...generateValidationProps('accountId', errors, touched)}
      />
      {showAuthorField && (
        <FormSelect
          name="authorId"
          options={mockAuthors}
          resolveId={(item) => item.id}
          resolveContent={(item) => item.name}
          top="Автор"
          placeholder="Кто создатель сбора?"
          {...generateValidationProps('authorId', errors, touched)}
        />
      )}

      <Button size="xl" type="submit">
        {submitButtonText}
      </Button>
    </FormLayout>
  )
}

interface Props {
  showAuthorField?: boolean
  submitButtonText: string
  targetPlaceholder: string
}

export default DonationForm
