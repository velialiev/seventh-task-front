// todo: костыль, временно помогающий упростить валидацию, пока существует баг в VKUI
// https://github.com/VKCOM/VKUI/issues/1012
import { ValidationStatus } from '../components/FormInput'

const generateValidationProps = (
  name: string,
  errors: Record<string, string | undefined>,
  touched: Record<string, boolean | undefined>,
) => {
  const error = errors[name]
  const isTouched = touched[name]

  return {
    status: isTouched && error ? ValidationStatus.Error : ValidationStatus.Default,
    bottom: isTouched && error ? error : '',
  }
}

export default generateValidationProps
