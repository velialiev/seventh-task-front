import React, {
  FC, useEffect, useRef, useState, 
} from 'react'
import {
  Div, getClassName, Text, usePlatform, 
} from '@vkontakte/vkui'
import { Icon28PictureOutline } from '@vkontakte/icons'
import './index.scss'
import { useField } from 'formik'
import cn from 'classnames'

const FileUploader: FC<Props> = ({ name }) => {
  const platform = usePlatform()
  const fileInputRef = useRef<HTMLInputElement>(null)
  const [field, , helper] = useField(name)
  const [preview, setPreview] = useState('')
  const fileUploaderClassName = cn(getClassName('FileUploader', platform), {
    'FileUploader--preview': preview,
  })

  const onFileInputClick = () => {
    if (!fileInputRef.current) {
      return
    }

    fileInputRef.current.click()
  }

  const onFileUpload = () => {
    if (!fileInputRef.current || !fileInputRef.current.files) {
      return
    }

    helper.setValue(fileInputRef.current.files[0])
  }

  useEffect(() => {
    if (!field.value) {
      setPreview('')
      return
    }

    setPreview(URL.createObjectURL(field.value))
  }, [field.value])

  return (
    <Div className={fileUploaderClassName} onClick={onFileInputClick}>
      {!preview && (
        <Text weight="regular" className="Text">
          <Icon28PictureOutline className="Text__icon" /> Загрузить обложку
        </Text>
      )}

      {preview && <img src={preview} alt="" className="FileUploader__preview" />}

      <input
        accept="image/*"
        type="file"
        style={{ display: 'none' }}
        ref={fileInputRef}
        onChange={onFileUpload}
      />
    </Div>
  )
}

interface Props {
  name: string
}

export default FileUploader
