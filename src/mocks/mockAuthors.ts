const mockAuthors = [
  {
    id: '1',
    name: 'Вели Алиев',
  },
  {
    id: '2',
    name: 'Али Алиев',
  },
  {
    id: '3',
    name: 'Дмитрий Московский',
  },
  {
    id: '4',
    name: 'Иван Касаткин',
  },
]

export default mockAuthors
