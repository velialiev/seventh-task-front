const mockCards = [
  {
    id: 1,
    name: 'Счёт VK Pay · 1234',
  },
  {
    id: 2,
    name: 'Счёт Tinkoff Bank · 131412',
  },
  {
    id: 3,
    name: 'Счёт Сбербанк · 9921',
  },
]

export default mockCards
