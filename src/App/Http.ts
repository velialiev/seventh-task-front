import axios, { AxiosResponse } from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_HOST,
})

console.log(process.env.REACT_APP_BACKEND_HOST)

const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms))

class Http {
  public static get<T>(url: string, params?: Record<string, string | number>) {
    return Promise.all([
      axiosInstance.get<Response<T>>(url, {
        params,
      }),
      delay(700),
    ]).then(([res]) => res as AxiosResponse<Response<T>>)
  }

  public static post<T>(url: string, body: any) {
    return axiosInstance.post<Response<T>>(url, body)
  }
}

export interface Response<T> {
  status: number
  data: T
  message: string | undefined
  error: string | undefined
}

export default Http
