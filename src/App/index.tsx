import React, { useState } from 'react'
import '@vkontakte/vkui/dist/vkui.css'
import DonationTypesPage from '../features/Donation/pages/DonationTypesPage'
import DonationsPage from '../features/Donation/pages/DonationsPage'
import RegularDonationPage from '../features/Donation/pages/RegularDonationPage'
import TargetDonationPage, {
  TargetPageContext,
} from '../features/Donation/pages/TargetDonationPage'
import DonationPage from '../features/Donation/pages/DonationPage'
import { Panel, Root, View } from '@vkontakte/vkui'
import AdditionalSettingsPage from '../features/Donation/pages/TargetDonationPage/AdditionalSettingsPage'
import { DonationCreateDto } from '../features/Donation/DonationService'
import bridge from '@vkontakte/vk-bridge'

export enum ViewId {
  Donation = 'Donation',
}

export enum PanelId {
  Donation = 'Donation',
  Donations = 'Donations',
  DonationTypes = 'DonationTypes',
  RegularDonation = 'RegularDonation',
  TargetDonation = 'TargetDonation',
  AdditionalSettings = 'AdditionalSettings',
}

export const RouterContext = React.createContext<RouterContextValue>({
  currentViewId: ViewId.Donation,
  currentPanelId: PanelId.Donations,
  goBack: () => {},
  goForward: () => {},
  history: [PanelId.Donations],
})

type RouterContextValue = {
  currentViewId: ViewId
  currentPanelId: PanelId
  goBack: () => void
  goForward: (panelId: PanelId, data?: any) => void
  history: PanelId[]
  data?: any
}

export const initialTargetDonationFormValue = {
  image: undefined,
  name: '',
  amount: undefined,
  target: '',
  description: '',
  accountId: '',
  authorId: '',
  endDate: '',
}

const App = () => {
  const [currentViewId] = useState<ViewId>(ViewId.Donation)
  const [currentPanelId, setCurrentPanelId] = useState<PanelId>(PanelId.Donations)
  const [history, setHistory] = useState<PanelId[]>([PanelId.Donations])
  const [routerData, setRouterData] = useState()
  const [targetDonationFormValue, setTargetDonationFormValue] = useState<DonationCreateDto>(
    initialTargetDonationFormValue,
  )

  const goBack = () => {
    const updatedHistory = [...history]
    updatedHistory.pop()
    const panelId = updatedHistory[updatedHistory.length - 1]

    if (panelId === PanelId.Donations) {
      bridge.send('VKWebAppDisableSwipeBack')
    }

    setHistory(updatedHistory)
    setCurrentPanelId(panelId)
  }

  const goForward = (panelId: PanelId, data?: any) => {
    const updatedHistory = [...history]
    updatedHistory.push(panelId)

    if (currentPanelId === PanelId.Donations) {
      bridge.send('VKWebAppEnableSwipeBack')
    }

    setHistory(updatedHistory)
    setCurrentPanelId(panelId)
    setRouterData(data)
  }

  return (
    <RouterContext.Provider
      value={{
        currentViewId,
        currentPanelId,
        data: routerData,
        goBack,
        goForward,
        history,
      }}
    >
      <TargetPageContext.Provider
        value={{
          targetDonationFormValue,
          setTargetDonationFormValue,
        }}
      >
        <Root activeView={currentViewId}>
          <View id={ViewId.Donation} activePanel={currentPanelId}>
            <Panel id={PanelId.Donations}>
              <DonationsPage />
            </Panel>

            <Panel id={PanelId.Donation}>
              <DonationPage />
            </Panel>

            <Panel id={PanelId.DonationTypes}>
              <DonationTypesPage />
            </Panel>

            <Panel id={PanelId.RegularDonation}>
              <RegularDonationPage />
            </Panel>

            <Panel id={PanelId.TargetDonation}>
              <TargetDonationPage />
            </Panel>

            <Panel id={PanelId.AdditionalSettings}>
              <AdditionalSettingsPage />
            </Panel>
          </View>
        </Root>
      </TargetPageContext.Provider>
    </RouterContext.Provider>
  )
}

export default App
