import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Req,
  Request,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common'
import { DonationService } from '../services/DonationService'
import { DonationCreateDto } from '../dtos/DonationCreateDto'
import { extractIdFromInsertResult } from '../../../utils/extractIdFromInsertResultUtil'
import { FileInterceptor } from '@nestjs/platform-express'
import { diskStorage } from 'multer'
import { existsSync, mkdirSync } from 'fs'
import { v4 as uuid } from 'uuid'
import path, { extname } from 'path'
import { Response } from 'express'

@Controller('/donation')
export class DonationController {
  constructor(private donationService: DonationService) {}

  @Get('/')
  public find() {
    return this.donationService.find()
  }

  @Get('/:id')
  public async findById(@Param('id') id: string) {
    if (!id) {
      throw new BadRequestException("id isn't provided")
    }
    const donation = await this.donationService.findById(+id)

    if (!donation) {
      throw new NotFoundException('donation not found')
    }

    return donation
  }

  @Post('/')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
          const uploadPath = './uploads'

          if (!existsSync(uploadPath)) {
            mkdirSync(uploadPath)
          }

          cb(null, uploadPath)
        },
        filename: (req: any, file: any, cb: any) => {
          cb(null, `${uuid()}${extname(file.originalname)}`)
        },
      }),
    }),
  )
  public async create(@Body() donationCreateDto: DonationCreateDto, @UploadedFile() image: any) {
    const result = await this.donationService.create({
      ...donationCreateDto,
      imageName: image ? image.filename : null,
    })
    return {
      id: extractIdFromInsertResult(result),
    }
  }

  @Get('/image/:name')
  public async getImageByName(@Param('name') name: string, @Res() res: Response) {
    res.sendFile(path.join(__dirname, '..', '..', '..', '..', '..', 'uploads', name))
  }
}
