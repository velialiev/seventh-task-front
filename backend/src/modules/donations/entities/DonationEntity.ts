import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm'
import { EntityId } from '../../../types/EntityId'

@Entity()
export class Donation {
  @PrimaryGeneratedColumn()
  public id: EntityId

  @Column()
  public accountId: string

  @Column()
  public authorId: string

  @Column()
  public amount: number

  @Column()
  public description: string

  @Column({ nullable: true })
  public endDate: string

  @Column()
  public name: string

  @Column()
  public target: string

  @Column({ nullable: true })
  public imageName: string
}
