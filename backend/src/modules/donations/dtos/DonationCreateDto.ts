import { IsNotEmpty } from 'class-validator'

export class DonationCreateDto {
  @IsNotEmpty()
  public accountId: string

  @IsNotEmpty()
  public authorId: string

  @IsNotEmpty()
  public amount: number

  @IsNotEmpty()
  public description: string

  public endDate: string

  @IsNotEmpty()
  public name: string

  @IsNotEmpty()
  public target: string

  public imageName: string
}
