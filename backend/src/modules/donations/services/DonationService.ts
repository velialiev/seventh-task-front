import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Donation } from '../entities/DonationEntity'
import { Repository } from 'typeorm'
import { DonationCreateDto } from '../dtos/DonationCreateDto'

@Injectable()
export class DonationService {
  constructor(
    @InjectRepository(Donation)
    private donationRepository: Repository<Donation>,
  ) {}

  public async find() {
    return this.donationRepository.find({
      order: {
        id: 'DESC',
      },
    })
  }

  public async findById(id: number) {
    return this.donationRepository.findOne(id)
  }

  public async create(donationCreateDto: DonationCreateDto) {
    return this.donationRepository.insert(donationCreateDto)
  }
}
