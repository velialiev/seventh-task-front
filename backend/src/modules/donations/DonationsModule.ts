import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Donation } from './entities/DonationEntity'
import { DonationService } from './services/DonationService'
import { DonationController } from './controllers/DonationController'

@Module({
  imports: [TypeOrmModule.forFeature([Donation])],
  providers: [DonationService],
  controllers: [DonationController],
})
export class DonationsModule {}
