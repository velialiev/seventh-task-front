import { InsertResult } from 'typeorm/index'
import { EntityId } from '../types/EntityId'

export const extractIdFromInsertResult = (insertResult: InsertResult): EntityId => {
  const {
    identifiers: [{ id }],
  } = insertResult
  return id
}
