import { Controller, Get } from '@nestjs/common'

@Controller('/')
export class RootController {
  // important for Uptime Monitor
  @Get('/')
  public hi() {
    return {
      message: 'hi',
    }
  }
}
